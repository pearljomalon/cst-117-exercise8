using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Pearl Jomalon
//CT 117
//Exercise 8
//May 10, 2019



namespace Exercise8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calcFatButton_Click(object sender, EventArgs e)
        {
            //testing for double
            double tstDbl;
            if (!double.TryParse(fatGramsTextBox.Text, out tstDbl))
            {
                //display message when doule not entered
                MessageBox.Show("Enter Valid Double", "Input Error");
                fatGramsTextBox.Text = "";
                fatGramsTextBox.Focus();
            }

            else
            {
                //calculate calories from fat
                fatCaloriesTextBox.Text = Convert.ToString(FatCalories(tstDbl));
            }
        }

        private void calcCarbsButton_Click(object sender, EventArgs e)
        {
            //testing for double
            double tstDbl;
            if (!double.TryParse(carbsGramsTextBox.Text, out tstDbl))
            {
                //display message when doule not entered
                MessageBox.Show("Enter Valid Doubl", "Input Error");
                carbsGramsTextBox.Text = "";
                carbsGramsTextBox.Focus();
            }

            else
            {
                //calculate calories from carbs
                carbsCaloriesTextBox.Text = Convert.ToString(CarbCalories(tstDbl));
            }
        }

        //convert fat grams to calories
        public double FatCalories(double fatGram)
        {
            return fatGram * 9.0;
        }

        //convert carb grams to calories
        public double CarbCalories(double carbGrams)
        {
            return carbGrams * 4.0;
        }

        //close program
        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
